<?php

namespace Tokenizer\Reader;

use Tokenizer\Helper\StringHelper;

class FileReader
{
    /**
     * file path
     * @var string
     */
    protected $filePath;

    /**
     * @param string $file
     */
    public function __construct($filePath)
    {
        $this->filePath=$filePath;
    }

    /**
     * get file content
     * @return string
     */
    public function getContent()
    {
        if(!file_exists($this->filePath)){
            throw new \Exception("File not found", 1);
        }
        return StringHelper::replaceEndLineAndSpace(file_get_contents($this->filePath));
    }
}
