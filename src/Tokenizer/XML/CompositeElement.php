<?php

namespace Tokenizer\XML;

use Tokenizer\Tokenizer;

abstract class CompositeElement implements IElement
{
    /**
     * element name
     * @var string
     */
    protected $name;

    /**
     * element text
     * @var string
     */
    protected $text;

    /**
     * inner node
     * @var string
     */
    protected $innerNode;

    /**
     * children
     * @var array
     */
    protected $children=array();

    /**
     * add child element
     * @param IElement $element child
     */
    public function add(IElement $element)
    {
        $this->children[]=$element;

        return $this;
    }

    /**
     * remove child element
     * @param  IElement         $element
     * @return CompositeElement
     */
    public function remove(IElement $element)
    {
        if ($key=$this->getKeyByElement($element)) {
            unset($this->children[$key]);
        }

        return $this;
    }

    /**
     * check if element has child
     * @return boolean
     */
    public function hasChild()
    {
        return count($this->children) > 0;
    }

    /**
     * get children
     * @return array
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * get child by index value
     * @param  int           $index
     * @return IElement|null
     */
    public function getChildByIndex($index)
    {
        if (isset($this->children[$index])) {
            return $this->children[$index];
        }

        return null;
    }

    /**
     * set inner node
     * @param string $innerNode
     */
    public function setInnerNode($innerNode)
    {
        $this->innerNode=$innerNode;

        return $this;
    }

    /**
     * get inner node - for further processing of child element(s)
     * @return string
     */
    public function getInnerNode()
    {
        return $this->innerNode;
    }

    /**
     * set element text value
     * @param string $text
     */
    public function setText($text)
    {
        $this->text=$text;

        return $this;
    }

    /**
     * get element text value
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * set element name
     * @param string $name
     */
    public function setName($name)
    {
        $this->name=$name;

        return $this;
    }

    /**
     * get element name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * transform IElement to simple standard object
     * @return \stdClass
     */
    public function toSimpleObject()
    {
        $obj=new \stdClass();
        $obj->name=$this->name;
        $obj->value=$this->text;
        $obj->children=array();
        foreach ($this->children as $key => $child) {
            $obj->children[]=$child->toSimpleObject();
        }

        return $obj;
    }
}
