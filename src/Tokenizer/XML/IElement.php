<?php

namespace Tokenizer\XML;

interface IElement
{
    /**
     * add child
     * @param IElement $element
     */
    public function add(IElement $element);

    /**
     * get children
     * @return array
     */
    public function getChildren();

    /**
     * get inner node
     * @return array
     */
    public function getInnerNode();

    /**
     * convert to string
     * @return string
     */
    public function __toString();
}
