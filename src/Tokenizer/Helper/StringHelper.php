<?php

namespace Tokenizer\Helper;

class StringHelper
{
    /**
     * replace ending line characters and spaces between elements
     * @param  string $content
     * @return string replaced string
     */
    public static function replaceEndLineAndSpace($content)
    {
        return preg_replace('/(\n\s*)?(\r\s*)?/', null, $content);
    }
}
