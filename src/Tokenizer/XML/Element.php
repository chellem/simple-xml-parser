<?php

namespace Tokenizer\XML;

class Element extends CompositeElement
{
    /**
     * convert to string
     * @return string
     */
    public function __toString()
    {
        return $this->text;
    }
}
