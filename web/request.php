<?php
require_once __DIR__.'/../vendor/autoload.php';

use Tokenizer\Tokenizer;
use Tokenizer\Reader\FileReader;

$file=$_GET['file'];
$fullPath=__DIR__."/../resources/{$file}.xml";
if(file_exists($fullPath)){
    $filereader=new FileReader($fullPath);
    $tokens=Tokenizer::getInstance()
                        ->setContent($filereader->getContent())
                        ->build();
    header('Content-Type: application/json');
    echo json_encode($tokens->toSimpleObject());
}