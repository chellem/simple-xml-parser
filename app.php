<?php

require_once 'vendor/autoload.php';

use Tokenizer\Tokenizer;
use Tokenizer\Reader\FileReader;

if($argv && isset($argv[1])){
	try{
		$filereader=new FileReader("resources/{$argv[1]}.xml");
		$tokens=Tokenizer::getInstance()
		                    ->setContent($filereader->getContent())
		                    ->build();
		print_r($tokens);
	}catch(\Exception $e){
		echo $e->getMessage();
	}
}else{
	echo "missing arguments\n";
}