<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Tokenization Demo</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="assets/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="assets/css/main.css">

        <script src="assets/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Tokenization Demo</a>
        </div>
      </div>
    </div>
    <br /><br/>
    <div class="container">
    <select id="xmlSelect" class="form-control">
      <?php foreach (glob(__DIR__.'/../resources/*.xml') as $file): ?>
        <?php $fileInfo=new SplFileInfo($file);?>
        <option value="<?php echo $fileInfo->getBaseName('.xml'); ?>"><?php echo $fileInfo->getBaseName('.xml'); ?></option>
      <?php endforeach; ?>
    </select><br />
      <div id="data" class="well"></div>
    </div>
    <script id="my_template" type="text/j-query-tmpl">
        <dl>
          <dt>${name}</dt>
          {{if value}}
            <dd>${value}</dd>
          {{/if}}
        </dl>
        <ol>
            {{each(i, child) children}}
            <li>
              <dl>
                <dt>${child.name}</dt>
                {{if child.value}}
                  <dd>${child.value}</dd>
                {{/if}}
              </dl>
              {{if child.children}}
                  {{tmpl(child.children) "my_template_compiled"}}
              {{/if}}
            </li>
            {{/each}}
        </ol>
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

        <script src="assets/js/vendor/bootstrap.min.js"></script>
        <script src="assets/js/vendor/jquery.tmpl.min.js"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>