$(document).ready(function(){
    makeRequestAndRender('booklist');
    $('#xmlSelect').change(function(){
        makeRequestAndRender($(this).val());
    });

    function makeRequestAndRender(item){
        $.get('request.php?file=' + item,function(data){
            $('#data').html('');
            $("#my_template").template("my_template_compiled");

            $.tmpl("my_template_compiled", data).appendTo("#data");
        });
    }
});