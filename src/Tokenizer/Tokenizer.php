<?php

namespace Tokenizer;

use Tokenizer\XML\Element;
use Tokenizer\XML\IElement;

class Tokenizer
{
    /**
     * XML content
     * @var string
     */
    private $content;

    /**
     * Tokenized object
     * @var IElement
     */
    private $element;

    /**
     * Tokenizer instance
     * @var Tokenizer
     */
    private static $instance;

    private function __construct() { }

    private function __clone() { }

    /**
     * get XML content
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * set XML content
     * @param string $content
     */
    public function setContent($content)
    {
        $this->element=null;
        $this->content=$content;

        return $this;
    }

    /**
     * build tokens
     * @return IEelement
     */
    public function build()
    {
        return $this->getElement($this->content);
    }

    /**
     * get tokens
     * @param  string   $content xml content
     * @return IElement element object
     */
    public function getElement($content)
    {
        $this->removeProcessingInstruction()->removeComment();
        $start=false;
        $elementName=null;
        $values=$this->getElements($this->getRootElement());
        // getting root element
        if (!$this->element) {
            $this->element=current($values);
            $this->processChild($this->element);
        }

        return $this->element;
    }

    /**
     * process inner node
     * @param  Element $element
     * @return array
     */
    private function processChild(IElement $element)
    {
        $elements=[];
        $this->content=$element->getInnerNode();
        if ($elements=$this->getElements($this->getRootElement())) {
            foreach ($elements as $key => $child) {
                $this->content=self::removeElement($child->getName(), $this->content);
                $element->add($child);
            }
            while (preg_match('/</', $this->content)) {
                $rootElement=$this->getRootElement();
                $values=$this->getElements($rootElement);
                foreach ($values as $key => $value) {
                    $node=$this->findNodeInElement($rootElement, $this->element);
                    if ($node) {
                        $this->content=self::removeElement($value->getName(), $this->content);
                        $node->add($value);
                    }
                }
            }
        }

        return $elements;
    }

    /**
     * find element in child nodes
     * @param  string   $node    node name
     * @param  IElement $element
     * @return IElement found element
     */
    private function findElementInChild($node,IElement $element)
    {
        foreach ($element->getChildren() as $key => $child) {
            if ($childElement=$this->findNodeInElement($node, $child)) {
                $element=$childElement;
                break;
            }
        }
        $this->replaceInnerNode($node, $element);

        return $element;
    }

    /**
     * find node in element
     * @param  string        $node
     * @param  IElement      $element
     * @return IElement|null
     */
    private function findNodeInElement($node,IElement $element)
    {
        if (preg_match("/<{$node}\s*\/?>/", $element->getInnerNode())) {
            return $this->replaceInnerNode($node, $element)
                        ->findElementInChild($node, $element);
        }

        return null;
    }

    /**
     * replace inner node
     * @param  string    $node
     * @param  IElement  $element
     * @return Tokenizer
     */
    private function replaceInnerNode($node,IElement $element)
    {
        $innerNode=self::removeElement($node, $element->getInnerNode());
        $element->setInnerNode($innerNode);

        return $this;
    }

    /**
     * get root element from XML content
     * @return string matched element
     */
    private function getRootElement()
    {
        $match=[];
        if (!preg_match('/<([\w]+)\s*>/', $this->content, $match)) {
            preg_match('/<([\w]+)\s*\/>/', $this->content, $match);
        }

        return end($match);
    }

    /**
     * get nested elements from element name
     * @param  string $elementName
     * @return array  elements
     */
    private function getElements($elementName)
    {
        $matches=[];
        $elements=[];
        if ($elementName[0]=='/' || !$elementName) {
            return null;
        }
        if (!preg_match_all("/<{$elementName}\s*>(.*?)<\/{$elementName}>/", $this->content, $matches)) {
            if (preg_match("/<{$elementName}\s*\/>/", $this->content, $matches)) {
                $element=new Element();
                $element->setName($elementName);
                $elements[]=$element;
                $this->content=$this->removeElement($elementName, $this->content);

                return $elements;
            }
        }
        if (count(end($matches))>0 && count($elements)==0) {
            foreach (end($matches) as $key => $match) {
                $element=new Element();
                $element->setName($elementName);
                if (preg_match('/</', $match)) {
                    $element->setInnerNode($match);
                } else {
                    $element->setText($match);
                }
                $elements[]=$element;
            }
        }

        return $elements;
    }

    /**
     * Remove XML Processing Instruction
     * @return Tokenizer
     */
    private function removeProcessingInstruction()
    {
        $this->content=preg_replace('/(<\?xml\s*.*?\?>)/',null,$this->content);

        return $this;
    }

    /**
     * remove comments in XML
     * @return Tokenizer
     */
    private function removeComment()
    {
        $this->content=preg_replace('/(<!--.*-->)/',null,$this->content);

        return $this;
    }

    /**
     * remove element from XML content
     * @param  string $element
     * @param  string $text    XML content
     * @return string remaining XML content
     */
    private static function removeElement($element, $text)
    {
        if ($element[0]=='/') {
            return $text;
        }
        $text=preg_replace("/<{$element}\s*>/", null, $text, 1);
        $text=preg_replace("/<\/{$element}>/", null, $text, 1);
        $text=preg_replace("/<{$element}\s*\/>/", null, $text, 1);

        return $text;
    }

    /**
     * get tokenizer instance
     * @return Tokenizer
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance=new Self();
        }

        return self::$instance;
    }
}
